<?php
/*
 * Copyright (c) 2019 Elouan Martinet <exa@elou.world>
 *
 * ewd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

header('content-type: text/plain;charset=utf-8');

function fatal(string $msg): void {
    http_response_code(403);
    die($msg . "\n");
}

if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] !== 'GET'
        || !isset($_SERVER['REQUEST_URI']))
    fatal('not GET');

$filename = strrchr($_SERVER['REQUEST_URI'], '/');

if ($filename !== false) {
    $filename = substr($filename, 1);
    $pos = strpos($filename, '.');
    if ($pos !== false)
        $filename = substr($filename, 0, $pos);
    $filename = __DIR__ . '/' . $filename;
}

$config = require __DIR__ . '/config.php';

if ($filename === false || !file_exists($filename) || ($sb = stat($filename)) === false)
    fatal('not found');

if ($sb['size'] > $config['link_max_size'])
    fatal('file too big');

$link = file_get_contents($filename);
if ($link === false)
    fatal('cannot open file');

$link = trim($link);
if (filter_var($link, FILTER_VALIDATE_URL) === false)
    fatal('not a valid link');

header('location: ' . $link);
echo $link . "\n";

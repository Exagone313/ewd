<?php
return [
    'link' => 'https://example.org',
    'tokens' => [
        'authorization_token_example',
    ],
    'users' => [
        'foo' => '$argon2id$v=19$m=65536,t=4,p=1$WGJkbTNYYzgvWHpMSHV2Zg$SiEZaCtayMMjVizAdMF+PekW25u8rKawdV4fm/k1h6M',
    ],
    'file_max_size' => 1024 * 2014,
    'extension_max_length' => 7,
    'link_max_size' => 256,
];

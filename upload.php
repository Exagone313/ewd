<?php
/*
 * Copyright (c) 2019 Elouan Martinet <exa@elou.world>
 *
 * ewd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

header('content-type: text/plain;charset=utf-8');

function fatal(string $msg): void {
    http_response_code(403);
    die($msg . "\n");
}

if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] !== 'POST')
    fatal('not POST');

if (!isset($_SERVER['HTTP_AUTHORIZATION']))
    fatal('not allowed');

$config = require __DIR__ . '/config.php';
$credentials = explode(':', $_SERVER['HTTP_AUTHORIZATION'], 2);

do {
    if (count($credentials) !== 2) {
        if (array_key_exists('tokens', $config)
            && in_array($_SERVER['HTTP_AUTHORIZATION'], $config['tokens'])) // deprecated
            break;
    }
    else {
        if (array_key_exists('users', $config)
            && strlen($credentials[0]) <= 64
            && strlen($credentials[1]) <= 256
            && array_key_exists($credentials[0], $config['users'])) {
            if (!password_verify($credentials[1], $config['users'][$credentials[0]]))
                fatal('not allowed');
            break;
        }
    }
    password_hash('fake-ewd-password-string', PASSWORD_ARGON2ID);
    fatal('not allowed');
} while(false);

$file = current($_FILES);

if ($file === false)
    fatal('no file sent');

if ($file['error'] !== UPLOAD_ERR_OK)
    fatal('upload error (' . $file['error'] . ')');

if ($file['size'] > $config['file_max_size'])
    fatal('file too big (max: ' . $config['file_max_size'] . ' bytes)');

function slug(): string {
    return bin2hex(random_bytes(3));
}

do {
    $slug = slug();
    $filename = __DIR__ . '/' . $slug;
} while (file_exists($filename));

if (!move_uploaded_file($file['tmp_name'], $filename))
    fatal('cannot save file');

$extension = strrchr($file['name'], '.');
if ($extension === false || strlen($file['name']) <= 1
        || strlen($extension) > $config['extension_max_length'])
    $extension = '.txt';

echo $config['link'] . '/' . $slug . $extension . "\n";
